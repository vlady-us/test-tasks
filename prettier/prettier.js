function prettier(val) {

    let correctValRegExp = /[^0-9.]/g;
    let sign = parseFloat(val) < 0 ? '-' : '';

    val = Math.abs(parseFloat(val)).toFixed(2);

    if (correctValRegExp.test(Math.abs(val)) || isNaN(val)) {
        return new Error('incorrect value');
    }

    return sign + (val.replace(/(?=(\d{3})+(?!\d))/g, " ")).trim();

}

let arr =  ['312313.00','3123123',313123.10,'-322123123123',1000,-180, true, {}, [], null, undefined]

for(let i of arr) {
    console.log(`${i}  \t - \t  ${prettier(i)}`)
}