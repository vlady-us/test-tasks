import Vue from 'vue'

import UsersComponent from "@/components/content/UsersComponent";
import FavouriteUsersComponent from "@/components/content/FavouriteUsersComponent";
import UserComponent from "@/components/content/UserComponent";
import PageNotFoundComponent from "@/components/content/PageNotFoundComponent";

import VueRouter from "vue-router";

Vue.use(VueRouter);


export default new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', component: UsersComponent},
    {path: '/favourites', component: FavouriteUsersComponent},
    {name: 'user', path: '/user/:login', component: UserComponent},
    {path: '*', component: PageNotFoundComponent}
  ]
})
