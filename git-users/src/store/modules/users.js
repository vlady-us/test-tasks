export default {
  state: {
    items: []
  },
  mutations: {
    updateUsers(state, user) {
      let removeIndex = state.items.map(function (item) {
        return item.login
      })
          .indexOf(user.login);

      ~removeIndex && state.items.splice(removeIndex, 1);
    },
    setUser(state, user) {

      let isUserExist = state.items.find(customUser => {
        return customUser.login === user.login
      });

      if (!isUserExist) {
        state.items.push({
          'id': user.id,
          'login': user.login,
          'avatar_url': user.avatar_url,
          'html_url': user.html_url,
          'isFavourite': true
        })
      }
    },
    removeAllFavouritesUsers(state) {
      if (state.items.length) {
        state.items = [];
      }
    }
  },
  getters: {
    users: state => {
      return state.items;
    }
  }
};
