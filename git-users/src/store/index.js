import Vue from 'vue';
import Vuex from 'vuex';

import usersModule from './modules/users';

Vue.use(Vuex);


import VuexPersist from 'vuex-persistedstate'

const vuexPersist = new VuexPersist({
  key: 'app',
  storage: window.localStorage
});

export default new Vuex.Store({
  modules: {
    users: usersModule,
  },
  getters: {},
  plugins: [vuexPersist]
});
