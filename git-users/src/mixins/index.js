import {eventBus} from "@/main";

export const mixins = {
  methods: {
    addToFavourites: function (user) {
      this.$store.commit('setUser', user);
    },
    removeFromFavourites: function (user) {
      this.$store.commit('updateUsers', user)
    },
    removeAllFavourites: function () {
      this.$store.commit('removeAllFavouritesUsers');
    },
    searchByIDandLogin: function (users, word) {
      return users.filter(user => {
        return user.login.toLowerCase().includes(word.toLowerCase()) || (user.id + '').includes(word);
      })
    },
    updateSearch: function () {
      eventBus.$emit('updateSearch', {
        search: this.search
      })
    }

  }
};
