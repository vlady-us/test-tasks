# README #

Тестовые задания согласно [ТЗ](https://docs.google.com/document/d/1lFq08wDlhUCkWJapfJz9c56K2Kx6yaKqCKpo5kTnqBw/edit?usp=sharing)

### Задания

* [prettier] - Чистый JS:  написать притифаер для  суммы "3123123.00" -> "3 123 123.00"
* [mask] - Чистый JS: сделать маску ввода на  инпуте например для суммы с копейками *.00 (если отличается от маски то не вводится ничего) Вместо  * любое количество цифр, там где 00 (это две любые цифры)
* [git-users] Мини spa на vue с использованием grid

